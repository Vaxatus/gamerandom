import GameLogic
import FileUtility
import MainMenu


class Menu:
    attempts = 0

    def __init__(self):
        self.game = GameLogic.GameLogic()

    def getHeader(self):
        print("Program wylosowal liczbe z przedzialu 0 - 100")

    def startLogic(self):
        self.attempts += 1
        print("Podaj liczbe z przedzialu 0 - 100")
        try:
            self.userNum = input()
            if(self.userNum == "exit"):
                returnedValue = self.userNum
            else:
                returnedValue = self.game.checkNumber(int(self.userNum))
        except:
            returnedValue = "nan"

        return self.validValue(returnedValue)


    def validValue(self, value):
        if value == "exit":
            print("Wyszedles z programu")
            return False
        if value == "nan":
            print("nie podales liczby")
        elif value == 0:
            print("Bravo odgadles poprawna liczbe - " + self.userNum)
            return False
        elif value == -1:
            print("Liczba jest za mala")
        elif value == 1:
            print("Liczba jest za duza")
        return True

    def start(self):
        start = True
        self.game.getRandomNumber()
        while start:
            self.getHeader()
            start = self.startLogic()

        FileUtility.addResults(self.attempts) # dodaje do tablicy
        MainMenu.MainMenu()
