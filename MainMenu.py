import FileUtility
import GameLogicMenu
import Results
class MainMenu:
    def __init__(self):
        start = True
        while start:
            start = self.startMainMenu()

    def startMainMenu(self):
        choseValue = self.getInput()
        if choseValue == 1:
            # go to GameLogicMenu
            GameLogicMenu.Menu().start()
        elif choseValue == 2:
            # go to Results
           Results.Results().showResulTs()
        elif choseValue == 3:
            FileUtility.writeToFile()  # zapisuje tablice do pliku
        return False

    def getInput(self):
        repeat = True
        while repeat:
            print("Podaj liczbe z menu: \n1.Start Game\n2.Pokaz Ranking\n3.Exit")
            returnValue = input()
            try:
                returnValue = int(returnValue)
                if returnValue < 1 or returnValue > 3:
                    print("Podaj liczbe z MENU")
                else:
                    repeat = False
            except:
                print("Podales zla wartosc")
        return returnValue