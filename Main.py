import FileUtility
import MainMenu


def initApp():
    FileUtility.createFile()
    FileUtility.readFromFile()


if __name__ == "__main__":
    initApp()
    MainMenu.MainMenu()
